<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/','Auth\LoginController@showLoginForm');
Route::get('/',function (){
    return redirect()->route('login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::group(['prefix' => 'state', 'as' => 'state.'], function () {
    Route::get('', 'StateController@index')->name('index');
    Route::get('create', 'StateController@create')->name('create');
    Route::post('store', 'StateController@store')->name('store');
    Route::get('{state}/edit', 'StateController@edit')->name('edit');
    Route::get('{state}/update', 'StateController@update')->name('update');
});
Route::group(['prefix' => 'city', 'as' => 'city.'], function () {
    Route::get('', 'CityController@index')->name('index');
    Route::get('create', 'CityController@create')->name('create');
    Route::post('store', 'CityController@store')->name('store');
    Route::get('{city}/edit', 'CityController@edit')->name('edit');
    Route::get('{city}/update', 'CityController@update')->name('update');
});