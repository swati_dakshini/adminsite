<?php

namespace App\Http\Controllers;

use App\Models\City;
use Illuminate\Http\Request;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;
use Yajra\DataTables\DataTables;

class CityController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax() || $request->has('export')) {
            $cityQuery = QueryBuilder::for(City::class)
                ->defaultSort('-id')
                ->allowedFilters([
                    'created_at',
                    AllowedFilter::exact('status'),
                    AllowedFilter::exact('state.id'),
                ])
                ->get();


            if ($request->ajax()) {
                return DataTables::of($cityQuery)
                    ->addColumn('action', 'city.datatable.action')
                    ->addColumn('status', 'city.datatable.status')
                    ->rawColumns(['action', 'status'])
                    ->addIndexColumn()
                    ->toJson();
            }
            /*if ($request->has('export')) {
                if ($cityQuery->count() > 1000) {
                    return redirect()->back()->with(['error' => 'Export has more than 1000 records. Please narrow your search filters.']);
                }

                return (new CategoryExport($cityQuery))->download('Category-' . now() . '.csv', Excel::CSV);
            }*/
        }
        return view('city.index');
    }

    public function create(){
        dd('create');
    }
}
