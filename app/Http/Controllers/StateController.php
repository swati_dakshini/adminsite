<?php

namespace App\Http\Controllers;

use App\Models\State;
use Illuminate\Http\Request;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;
use Yajra\DataTables\DataTables;

class StateController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax() || $request->has('export')) {
            $stateQuery = QueryBuilder::for(State::class)
                ->defaultSort('-id')
                ->allowedFilters([
                    'created_at',
                    AllowedFilter::exact('status'),
                    AllowedFilter::exact('state.id'),
                ])
                ->get();


            if ($request->ajax()) {
                return DataTables::of($stateQuery)
                    ->addColumn('action', 'state.datatable.action')
                    ->addColumn('status', 'state.datatable.status')
                    ->rawColumns(['action', 'status'])
                    ->addIndexColumn()
                    ->toJson();
            }
            /*if ($request->has('export')) {
                if ($stateQuery->count() > 1000) {
                    return redirect()->back()->with(['error' => 'Export has more than 1000 records. Please narrow your search filters.']);
                }

                return (new CategoryExport($stateQuery))->download('Category-' . now() . '.csv', Excel::CSV);
            }*/
        }
        return view('state.index');
    }

    public function create(){
       return view('state.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|unique:states,name',
        ], [
            'name.required' => 'The State Name field is required.'
        ]);

        State::Create([
            'name' => $request->get('name'),
            'status' => State::ACTIVE,
        ]);

        return redirect()->route('state.index')->with(['success' => 'State Created Successfully']);
    }
}
