<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class City extends Model
{
    protected $guarded = ['id'];

    const ACTIVE = 1;
    const INACTIVE = 0;
    public function state(){
        return $this->belongsTo(State::class);
    }
}
