<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    protected $guarded = ['id'];
    const ACTIVE =1;
    const INACTIVE = 0;
    /**
     * @return HasMany|City
     */
    public function cities()
    {
        return $this->hasMany(City::class);
    }
}
