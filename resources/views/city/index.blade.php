@extends('layouts.home_basic')

@section('main')

    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">City</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">City</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <div class="row">
        <div class="col-12"><div class="card mt-2">
                <div class="card-header bg-dark py-3 text-white">
                    <div class="card-widgets">
                        <div class="btn-group btn-group-sm" role="group">
                            <a href="{{ route('city.create') }}"
                               class="btn btn-secondary waves-effect waves-light font-weight-bold">
                                <i class="fe-plus font-weight-bold"></i> Create
                            </a>
                        </div>
                    </div>
                    <h5 class="card-title mb-0 text-white">City</h5>
                </div>

                </div>
        </div>
        <div class="col-12">
            <div class="card-box">
                <div class="table-responsive">
                    <table class="table table-bordereless table-striped responsive" id="cityTable">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Date</th>
                            <th>Name</th>
                            <th>Status</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>

    </div>

@endsection
@push('page-javascript')
<link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
<link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
<script>
    var dataTable = $('#cityTable').DataTable({
        orderCellsTop: true,
        fixedHeader: true,
        processing: true,
        serverSide: true,
        ajax: {
            url: '{{ route("city.index") }}',
        },
        columns: [
            {data: 'DT_RowIndex', searchable: false, orderable: false},
            {data: 'created_at', name: 'created_at'},
            {data: 'name', name: 'name'},
            {data: 'status', name: 'status', searchable: false, orderable: false},
        ]
    });
</script>
@endpush