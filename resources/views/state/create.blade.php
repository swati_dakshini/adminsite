@extends('layouts.home_basic')

@section('main')

    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">City</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">State</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <div class="row">
        <div class="col-12"><div class="card mt-2">
                <div class="card-header bg-dark py-3 text-white">
                    <div class="card-widgets">
                        <div class="btn-group btn-group-sm" role="group">
                            <a href="{{ route('state.index') }}"
                               class="btn btn-secondary waves-effect waves-light font-weight-bold">
                                <i class="mdi mdi-eye"></i> View
                            </a>
                        </div>
                    </div>
                    <h5 class="card-title mb-0 text-white">State</h5>
                </div>
                <div class="card-body">
                    <form action="{{ route('state.store') }}" method="post">
                        @csrf
                        <div class="row">
                            <div class="form-group col-xl-12 col-lg-12 col-md-12 col-12">
                                <label class="required">State Name </label>
                                <input type="text" name="name" class="form-control"
                                       placeholder="Brand Name" required>
                                @foreach($errors->get('name') as $error)
                                    <span class="text-danger">{{ $error }}</span>
                                @endforeach
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-12">
                                <a href="" class="btn btn-danger waves-effect waves-light font-weight-bold">
                                    <i class="mdi mdi-reload mr-1 font-weight-bold"></i> Reset
                                </a>
                                <button type="submit" name="submit" value="submit"
                                        class="btn btn-primary waves-effect waves-light font-weight-bold">
                                    <i class="mdi mdi-send mr-1 font-weight-bold mr-1"></i> Submit
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>

@endsection
