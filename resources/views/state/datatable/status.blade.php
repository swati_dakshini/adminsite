@if($model->status == \App\Models\City::ACTIVE)
    <span class="btn btn-success btn-xs waves-effect waves-light font-weight-bold"> Active </span>
@endif
@if($model->status == \App\Models\City::INACTIVE)
    <span class="btn btn-danger btn-xs waves-effect waves-light font-weight-bold"> Inactive </span>
@endif